<?php
/**
 * @file
 * Contains \Drupal\content_moderation_scheduler\EventSubscriber\SchedulerEventSubscriber.
 */

namespace Drupal\content_moderation_scheduler\EventSubscriber;

use Drupal\scheduler\SchedulerEvent;
use Drupal\scheduler\SchedulerEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
* Event Subscriber SchedulerEventSubscriber.
*/
class SchedulerEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[SchedulerEvents::PRE_PUBLISH][] = array('getNode');
    return $events;
  }

  /**
   * @param GetResponseEvent $event
   */
  public function getNode(SchedulerEvent &$event) {
    $node = $event->getNode();
    if (empty($node->publish_on->value)) {

      // get the latest revision with publish_on data
      $vids = \Drupal::entityQuery('node')
        ->exists('publish_on')
        ->condition('publish_on', REQUEST_TIME, '<=')
        ->condition('nid', $node->id())
        ->allRevisions()
        ->sort('publish_on', 'DESC')
        ->range(0, 1)
        ->execute();
      $vid = array_keys($vids)[0];
      $node = \Drupal::entityTypeManager()->getStorage('node')->loadRevision($vid);

      $event->setNode($node);
    }
  }

}