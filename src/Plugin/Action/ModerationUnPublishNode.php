<?php

namespace Drupal\content_moderation_scheduler\Plugin\Action;

use Drupal\content_moderation\StateTransitionValidationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Plugin\Action\PublishNode;
use Drupal\content_moderation\ModerationInformationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Alternate action plugin that can opt-out of modifying moderated entities.
 *
 * @see \Drupal\node\Plugin\Action\PublishNode
 */
class ModerationUnPublishNode extends PublishNode implements ContainerFactoryPluginInterface {

  /**
   * Moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;
  protected $entityTypeManager;
  protected $validation;

  /**
   * ModerationOptOutPublishNode constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_info
   *   The moderation information service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModerationInformationInterface $moderation_info,
                              StateTransitionValidationInterface $validation, EntityTypeManager $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moderationInfo = $moderation_info;
    $this->validation = $validation;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('content_moderation.moderation_information'),
      $container->get('content_moderation.state_transition_validation'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // if the entity is moderated, set the latest revision to the published state
    if ($entity && $this->moderationInfo->isModeratedEntity($entity)) {

      $states = $this->getAvailableStates();
      $entity->get('moderation_state')->target_id = $states['archived']->id();
      $entity->save();
      return;
    }
    parent::execute($entity);
  }

  /**
   * @return \Drupal\content_moderation\ModerationStateInterface[]
   */
  protected function getAvailableStates() {
    return $this->entityTypeManager->getStorage('moderation_state')
      ->loadMultiple();
  }

}
